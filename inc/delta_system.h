//
//  delta_system.h
//  delta_macos
//
//  Created by Justin on 15/06/13.
//  Copyright (c) 2013 Justin. All rights reserved.
//

#ifndef __DELTA_SYSTEM_H__
#define __DELTA_SYSTEM_H__

#ifndef __DELTA_MAIN_H__
#error "File can't be included directly."
#endif

// system utilities and defines
#include <assert.h>
#ifdef DELTA_DEBUG
    #define DELTA_ASSERT(x) assert(x);
#else
    #define DELTA_ASSERT(x)
#endif

// TODO
#define THREADLOCAL

// TODO move to separate file
#include <mach/mach_time.h>
namespace Delta
{
    // Get the number of system ticks since beginning of system time.
    inline uint64_t GetSystemTickCount()
    {
        return mach_absolute_time();
    }

    // Get the period of a system tick in seconds.
    inline double GetSystemTickPeriod()
    {
        // Use timebase info to account for differences between architecture's
        // tick time.
        mach_timebase_info_data_t info;
        mach_timebase_info(&info);
        
        return double(info.numer) / ( double(info.denom) * 1e9 );
    }
}

#endif // __DELTA_SYSTEM_H__
