#ifndef __DELTA_MAIN_H__
#define __DELTA_MAIN_H__

// Main header file for DELTA, client code should include this file only.

// User code may chose to include nothing of detla by setting DELTA_INCLUDED=0
// Note DELTA_ENABLED is to compile in instrumentation calls. *_INCLUDED is just
// a super safe outlet to make sure user may completely remove all source,
// without having to modify client side #includes.

#if !defined(DELTA_INCLUDED) || (DELTA_INCLUDED!=0)
    #include "delta_config.h"        // set up #defines for this environment

    #include "delta_system.h"        // system level functionality (timers, threads, asserts)
    #include "delta_zone_registry.h" // the main code for instrumentation
    #include "delta_stream_recorder.h"
    #include "delta_macros.h"        // user facing interface macros
#endif

// Have a nice day.
#endif // __DELTA_MAIN_H__