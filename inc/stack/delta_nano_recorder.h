//
//  delta_nano_recorder.h
//  delta_macos
//
//  Created by Justin on 15/06/13.
//  Copyright (c) 2013 Justin. All rights reserved.
//

#ifndef __DELTA_NANO_RECORDER_H__
#define __DELTA_NANO_RECORDER_H__

#include "delta_zone_registry.h"
#include "delta_zone_stack.h"

namespace Delta
{
    class NanoRecorder
	{
	public:
		NanoRecorder(ZoneStack&    a_stack,
                     uint32_t      a_numFrames);
        
		~NanoRecorder();
		
		void        EnterZone( ZoneId );
		void        LeaveZone( ZoneId );
		void        Update();
		
		// Get a sample from a given frame and zone
		uint64_t	GetSample( size_t a_frame, ZoneId a_zone ) const;
		
		// Get total time across all zones on a given frame
		uint64_t	GetFrameTotal( size_t a_frame ) const;
		
		// Get the current frame index
		size_t		GetLastFrame() const;
        
        uint64_t    GetSample(size_t a_frame, ZoneId a_zone) { return Sample(a_frame, a_zone); }
        
	private:
		uint64_t&	Sample( size_t a_frame, ZoneId a_zone );
		void		Accumulate( ZoneId a_zi );
		
        // TODO pass in zone registry, don't assume it's static
        //ZoneRegistry&   m_zoneRegistry;
		ZoneStack&		m_zoneStack;
        
		// History array - is number of zones * number of frames
		uint64_t*		m_history;
		const size_t	m_numFrames;
		const size_t	m_historySize;
		
        size_t			m_currentFrame;
		uint64_t		m_lastTickCount;
	};
    
    NanoRecorder::NanoRecorder(ZoneStack& a_stack,
                               uint32_t   a_numFrames)
    :   m_zoneStack( a_stack ),
        m_history( NULL ),
        m_numFrames( a_numFrames ),
        m_historySize( a_numFrames * ZoneRegistry::GetZoneCount() ),
        m_currentFrame( 0 )
    {
        // Create history buffer
        m_history = new uint64_t[m_historySize];
        
        // Clear history
        for ( size_t i = 0 ; i < m_historySize; i++ )
        {
            m_history[ i ] = 0;
        }
    }
    
    NanoRecorder::~NanoRecorder()
    {
        delete[] m_history;
    }
    
    inline uint64_t& NanoRecorder::Sample( size_t a_frame, ZoneId a_zi )
	{
		const size_t offset = ZoneRegistry::GetZoneCount() * a_frame + a_zi;
		DELTA_ASSERT( offset < m_historySize );
		return m_history[ offset ];
	}

    inline void NanoRecorder::Accumulate( ZoneId a_zi )
	{
		// Address correct sample in history
		uint64_t&	sample = Sample( m_currentFrame, a_zi );
		
		// Update sample with accumulated ticks
		uint64_t currentTicks	= GetSystemTickCount();
		sample					+= currentTicks - m_lastTickCount;
		m_lastTickCount			= currentTicks;
	}
	
	inline void NanoRecorder::EnterZone( ZoneId a_zi )
	{
		Accumulate( m_zoneStack.CurrentZone() );
		m_zoneStack.EnterZone( a_zi );
	}
	
	inline void NanoRecorder::LeaveZone( ZoneId a_zi )
	{
		Accumulate( a_zi );
		m_zoneStack.LeaveZone( a_zi );
	}
} // namespace Delta



#endif // __DELTA_NANO_RECORDER_H__
