//
//  delta_zone_stack.h
//  delta_macos
//
//  Created by Justin on 15/06/13.
//  Copyright (c) 2013 Justin. All rights reserved.
//
#ifndef __DELTA_ZONE_STACK__
#define __DELTA_ZONE_STACK__

#ifndef __DELTA_MAIN_H__
#error "File can't be included directly."
#endif

#include "delta_zone_registry.h"

namespace Delta
{
    enum { MAX_ZONE_STACK_SIZE = 1024 };

    class ZoneStack
    {
    public:
        ZoneStack()
        :	m_top(0)
        {
            EnterZone(DEFAULT_ZONE_ID);
        }
        
        ~ZoneStack()
        {
            LeaveZone(DEFAULT_ZONE_ID);
        }
        
        inline ZoneId	CurrentZone();
        inline void		EnterZone(ZoneId a_zid);
        inline void		LeaveZone(ZoneId a_zid);
        
    private:
        // After static init. this is the stack
        ZoneId		m_stack[MAX_ZONE_STACK_SIZE];
        uint32_t	m_top;
    };

    inline ZoneId ZoneStack::CurrentZone()
    {
        return m_stack[m_top-1];
    }

    inline void ZoneStack::EnterZone(ZoneId a_zi)
    {
        DELTA_ASSERT( m_top < MAX_ZONE_STACK_SIZE + 1 );
        m_stack[ m_top++ ] = a_zi;
    }

    inline void	ZoneStack::LeaveZone(ZoneId a_zi)
    {
        DELTA_ASSERT( a_zi == CurrentZone() );
        DELTA_ASSERT( m_top > 0 );
        --m_top;
    }
} // namespace Delta

#endif // __DELTA_ZONE_STACK__
