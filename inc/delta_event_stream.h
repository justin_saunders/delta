//
//  delta_event_stream.h
//  delta_macos
//
//  Created by Justin on 15/06/13.
//  Copyright (c) 2013 Justin. All rights reserved.
//

#ifndef __DELTA_EVENT_STREAM_H__
#define __DELTA_EVENT_STREAM_H__

namespace Delta
{
    // An event is a 16 byte packet representing 
    //  the type - defined at compile time, ie a begin, end of a timing zone, a one off event with a time stamp,
    //  a pointer do data - whatever. Essentaily a function lookup for the rest of the data
    //  the id - just a predefined number for the zone
    //
    // In the future look at compressing timing packets into 8 bytes. Timing packets store
    // raw clock data, so do the math to check that 48 bits is enough precision. 
    struct Event
    {   
        // 8 bytes
        uint16_t type;
        uint16_t zoneId;
        uint32_t __unused__;

        // 8 bytes
        uint64_t data;  
    };
    
    
    // Event stream is a buffer of events with an optional callback when full.
    // The callback can copy the full buffer for further processing. There
    // should be one EventStream per thread.
    class EventStream
    {
    public:
        
        enum { MAX_EVENTS = 1024 };
        
        typedef void (*BufferFullCallback)(Event*, uint32_t);
        
        EventStream(BufferFullCallback notifyBufferFull = NULL)
            : m_pos(0),
              m_notifyBufferFull(notifyBufferFull)
        {
        }
        
        void AddEvent(const Event& a_newEvent)
        {
            // If buffer is full, notify and wrap
            m_pos += 1;
            
            if (m_pos == MAX_EVENTS)
            {
                if (m_notifyBufferFull)
                {
                    m_notifyBufferFull(m_buffer, MAX_EVENTS);
                }
                
                m_pos = 0;
            }
            
            // TODO PERF - ensure this an native quadword copy
            m_buffer[m_pos] = a_newEvent;
        }
        
    private:
        uint32_t            m_pos;
        Event               m_buffer[MAX_EVENTS];
        BufferFullCallback  m_notifyBufferFull;
    };
}

#endif // __DELTA_EVENT_STREAM_H__
