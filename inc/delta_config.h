#ifndef __DELTA_CONFIG_H__
#define	__DELTA_CONFIG_H__

// Supported architectures
#define DELTA_ARCH_ARM		0
#define DELTA_ARCH_X64		1

#if defined ( __x86_64__ )
    #define DELTA_ARCH_CURRENT DELTA_ARCH_X64
#elif defined ( __arm__ )
    #define DELTA_ARCH_CURRENT DELTA_ARCH_ARM
#else
    #error "This architecture is not supported by DELTA."
#endif

// Known platforms
#define DELTA_PLAT_IOS  0
#define DELTA_PLAT_OSX  1
#define DELTA_PLAT_WRT  2
#define DELTA_PLAT_WIN  3
#define DELTA_PLAT_LNX  4
#define DELTA_PLAT_AND  5
#define DELTA_PLAT_PSV  6
#define DELTA_PLAT_PS3  7
#define DELTA_PLAT_360  8
#define DELTA_PLAT_WII  9
#define DELTA_PLAT_WIU  10
#define DELTA_PLAT_XB1  11
#define DELTA_PLAT_PS4  12

// Supported platforms
#if defined ( __APPLE__ ) && defined ( __MACH__ )
	#if TARGET_OS_IOS
		#define DELTA_PLAT_CURRENT DELTA_PLAT_IOS
	#else
		#define DELTA_PLAT_CURRENT DELTA_PLAT_OSX
	#endif
#endif

#if !defined ( DELTA_PLAT_CURRENT )
	#error "This platform is not supported by DELTA"
#endif

// Debugging enabled (non release build)
#if !defined ( NDEBUG )
    #define DELTA_DEBUG
#endif

#endif // __DELTA_CONFIG_H__