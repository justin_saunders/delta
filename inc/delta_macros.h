#ifndef __DELTA_MACROS_H__
#define __DELTA_MACROS_H__

#ifndef __DELTA_MAIN_H__
    #error "File can't be included directly."
#endif

namespace Delta {
    // A handly scoped zone wrapper
    template <typename R> class ScopedZone
    {
    public:
        ScopedZone(R& a_recorder, ZoneId a_zi) : m_recorder(a_recorder), m_zoneId(a_zi)
        {
            m_recorder.EnterZone( m_zoneId );
        }
        
        ~ScopedZone()
        {
            m_recorder.LeaveZone( m_zoneId );
        }
        
    private:
        R&      m_recorder;
        ZoneId  m_zoneId;
    };
}

#if defined(DELTA_ENABLED) && (DELTA_ENABLED==0)
    // Declare zones and events before run time
    #define DELTA_ZONE_REGISTER(tag)
    #define DELTA_EVENT_REGISTER(tag)
   
    #define DELTA_INIT()                    // required per application before main
    #define DELTA_THREAD_INIT(thread_name)  // required per non-main thread
    
    // At run time, instrument code
    #define DELTA_ZONE_BEGIN(tag)
    #define DELTA_ZONE_END(tag)
    #define DELTA_ZONE_AUTO(tag)

    // Alloc, Dealloc and Frame are just inbuilt events
    #define DELTA_LOG_EVENT(tag, userInfo)
    #define DELTA_LOG_ALLOC(tag, size)
    #define DELTA_LOG_DEALLOC(tag); // tricky - can name match alloc?
    #define DELTA_LOG_FRAME(userInfo) // is frame an inbuilt event?

#else

    #define DELTA_THREAD_INIT(thread_name)\
        THREADLOCAL Delta::ZoneStack t_zoneStack;\
        THREADLOCAL Delta::NanoRecorder t_recorder(t_zoneStack, 100);

    #define DELTA_INIT() \
            DELTA_INIT_ZONE_REGISTRY;
            //DELTA_THREAD_INIT("Main thread");

    #define DELTA_ZONE_REGISTER( name ) \
    Delta::ZoneId DeltaZone_##name = Delta::ZoneRegistry::DeclareZone( #name );

    #define DELTA_ZONE_ENTER( name ) \
    extern Delta::ZoneId DeltaZone_##name; \
    g_pmonitor->EnterZone( DeltaZone_##name );

    #define DELTA_ZONE_LEAVE( name ) \
    extern Delta::ZoneId DeltaZone_##name; \
    g_pmonitor->LeaveZone( DeltaZone_##name );

    #define DELTA_ZONE_AUTO( name ) \
    extern Delta::ZoneId DeltaZone_##name; \
    Delta::ScopedZone<Delta::NanoRecorder> DeltaScoped_##name( Delta::t_recorder, DeltaZone_##name );

#endif // delta enabled

#endif // __DELTA_MACROS_H__
