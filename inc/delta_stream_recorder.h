//
//  delta_stream_recorder.h
//  delta_macos
//
//  Created by Justin on 16/06/13.
//  Copyright (c) 2013 Justin. All rights reserved.
//

#ifndef __DELTA_STREAM_RECORDER_H__
#define __DELTA_STREAM_RECORDER_H__

#include "delta_event_stream.h"

namespace Delta
{
    class NanoRecorder
	{
	public:
		NanoRecorder();
        
		~NanoRecorder();
		
		void        EnterZone( ZoneId );
		void        LeaveZone( ZoneId );
     
    private:
        EventStream m_stream;
    };
    
} // namespace Delta

#endif // __DELTA_STREAM_RECORDER_H__
