#ifndef __DELTA_ZONE_REGISTRY_H__
#define __DELTA_ZONE_REGISTRY_H__

#ifndef __DELTA_MAIN_H__
#error "File can't be included directly."
#endif

#include <stdint.h>

namespace Delta {
    
    typedef uint32_t ZoneId;
    
    enum { MAX_ZONE_COUNT  = 8192 };
    enum { DEFAULT_ZONE_ID = 0 };

    // This class is a true global singleton as it is valid for all execution
    // threads. It represents all the registered zones in the process.
    class ZoneRegistry
    {
    public:  
        static ZoneId		DeclareZone(const char* a_zoneName);
        static const char*	GetZoneName(ZoneId a_zid);
        static ZoneId		GetZoneCount();
        
        ZoneRegistry()
        {
            if (s_instanceCount != 0) 
            {
                // TODO error
            }
        }
        // All registered zones, filled during static initialization and indexed
        // by ZoneId.
        static const char*	s_zoneNames[MAX_ZONE_COUNT];
        static ZoneId		s_zoneCount;
        static int          s_instanceCount;
    };

    ZoneId ZoneRegistry::DeclareZone(const char* a_zoneName)
	{
		DELTA_ASSERT( s_zoneCount < MAX_ZONE_COUNT );
		s_zoneNames[ s_zoneCount ] = a_zoneName;
		return s_zoneCount++;
	}

    inline const char* ZoneRegistry::GetZoneName(ZoneId a_zid)
    {
        return s_zoneNames[a_zid];
    }

    inline ZoneId ZoneRegistry::GetZoneCount()
    {
        return s_zoneCount;
    }
} // delta

// Because we're header only, this must be created once, before main.
#define DELTA_INIT_ZONE_REGISTRY \
    int                 Delta::ZoneRegistry::s_instanceCount	= 0; \
    Delta::ZoneId       Delta::ZoneRegistry::s_zoneCount = 1; \
    const char*         Delta::ZoneRegistry::s_zoneNames[MAX_ZONE_COUNT] = {"Default"}; \
    namespace Delta { ZoneRegistry s_zoneRegistry; }

#endif // __DELTA_ZONE_REGISTRY_H__
