//
//  main.cpp
//  delta_macos
//
//  Created by Justin on 18/05/13.
//  Copyright (c) 2013 Justin. All rights reserved.
//
#include <iostream>
#include <unistd.h>

#include "../../inc/delta_main.h"
#include "../../inc/delta_event_stream.h"

DELTA_INIT();
DELTA_ZONE_REGISTER(OuterZone);
DELTA_ZONE_REGISTER(InnerZone);

int main(int argc, const char * argv[])
{
    assert(sizeof(Delta::Event) == 16);
    
    {
        DELTA_ZONE_AUTO(OuterZone);
        usleep(1000 * 100);
        
        {
            DELTA_ZONE_AUTO(InnerZone);
            usleep(1000 * 50);
        }
    }
    
    assert(Delta::ZoneRegistry::GetZoneCount() == 3);
    std::cout << "default=" << Delta::t_recorder.GetSample(0, 0) << "\n"
              << "outer=" << Delta::t_recorder.GetSample(0, 1) << "\n"
              << "inner=" << Delta::t_recorder.GetSample(0, 2) << "\n";
    
    return 0;
}

