#include <stdio.h>
#include "../inc/delta_main.h"

DELTA_INIT();
DELTA_ZONE_REGISTER(main);
DELTA_ZONE_REGISTER(loop);

int main()
{
    printf("ZoneCount = %d\n"
           "Zone[0]   = %s\n"
           "Zone[1]   = %s\n"
           "Zone[2]   = %s\n", 
                Delta::ZoneRegistry::s_zoneCount,
                Delta::ZoneRegistry::s_zoneNames[0],
                Delta::ZoneRegistry::s_zoneNames[1],
                Delta::ZoneRegistry::s_zoneNames[2]);

    return EXIT_SUCCESS;
}